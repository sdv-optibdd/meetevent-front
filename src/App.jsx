import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Accueil from "./components/Accueil.jsx";
import Header from "./components/Header.jsx";
import Login from "./components/Auth/Login.jsx";

export default function App() {

    const router = createBrowserRouter([
        {
            path: '/',
            element: <Accueil/>
        },
        {
            path: '/login',
            element: <Login/>
        }
    ])

    return (
        <>
            <Header/>
            <div className="px-5 max-w-7xl mx-auto">
                <RouterProvider router={router}/>
            </div>
        </>
    );
}