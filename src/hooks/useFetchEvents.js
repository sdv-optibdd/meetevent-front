import { useState, useEffect } from "react";
import { updateEvents } from "../store/store.js";
import { apiBaseURL } from "../config.js";

export function useFetchEvents(endpoint) {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        fetch(`${apiBaseURL}${endpoint}`)
            .then(response => response.json())
            .then(data => updateEvents(data))
            .catch(error => {
                setError('Une erreur est survenue. Veuillez réessayer.')
                console.error(error)
            })
            .finally(() => setIsLoading(false));
    }, [endpoint]);

    return { isLoading, error };
}
