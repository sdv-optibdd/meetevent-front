import { create } from "zustand";

export const useAppStore =  create((set) => ({
    events: [],
    filterPostalCode: "",
    updateFilterPostalCode (postalCode) {
        set({ filterPostalCode: postalCode })
    }
}))

export const updateEvents = (events) =>
    useAppStore.setState({ events: events })

