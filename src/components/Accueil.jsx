import SearchEventInput from "./Event/SearchEventInput.jsx";
import EventList from "./Event/EventList.jsx";
import SearchEventFilter from "./Event/SearchEventFilter.jsx";

export default function Accueil() {
    return (
        <>
            <main className="px-5 pt-5">
                <SearchEventInput/>
                <SearchEventFilter/>
                <EventList/>
            </main>
        </>
    )
}