export default function Loader({ isLoading = false }) {
    return (
        isLoading && (
            <div className="w-full flex items-center justify-center">
                <div className="loader-spinner"></div>
            </div>
        )
    );
}
