export default function EventCard({ event }) {
    return (
        <div
             className="flex flex-col gap-4 rounded border border-slate-200 p-4 shadow">
            <div className="flex gap-2">
                <div className="h-14 w-14 animate-pulse rounded bg-slate-200"></div>
                <div className="flex w-full flex-col gap-2">
                    <div className="w-full text-xl font-bold">{event.name}</div>
                    <div className="w-full">
                        {event.category}
                    </div>
                </div>
            </div>
            <div className="w-full text-base">
                {event.description}
            </div>
        </div>
    )
}