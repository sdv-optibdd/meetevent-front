import { useAppStore } from "../../store/store.js";

export default function SearchEventFilter () {

    const { filterPostalCode, updateFilterPostalCode } = useAppStore()

    function handleChange ( e ) {
        e.preventDefault()
        updateFilterPostalCode(e.target.value)
    }

    return (
        <>
            <div className="max-w-40">
                <input type="number" value={filterPostalCode} onChange={handleChange} className="form-control" placeholder="Code postal" />
            </div>
        </>
    )
}