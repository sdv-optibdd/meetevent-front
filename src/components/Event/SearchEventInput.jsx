import { useState } from "react";
import { updateEvents, useAppStore } from "../../store/store.js";
import Loader from "../Loader.jsx";
import { apiBaseURL } from "../../config.js";

export default function SearchEventInput() {
    const [ search, setSearch ] = useState('');
    const [ isLoading, setIsLoading ] = useState(false);
    const [ error, setError ] = useState(null);

    const { filterPostalCode } = useAppStore()

    const handleSearch = (e) => {
        setSearch(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setIsLoading(true)
        setError(null)

        fetch(`${apiBaseURL}/party?name=${search}&postalCode=${filterPostalCode}`)
            .then(response => response.json())
            .then(data => {
                updateEvents(data)
            })
            .catch(error => {
                setError('Une erreur est survenue. Veuillez réessayer.')
                console.error('Error:', error);
            }).finally(() => setIsLoading(false));
    }

    return (
        <>
            <form onSubmit={handleSubmit} className="form-group flex gap-1">
                <input
                    type="text"
                    value={search}
                    onChange={handleSearch}
                    role="search"
                    className="form-control"
                    placeholder="Ex: 94350, Jeux, Autour de moi…"
                />
                <button className="btn-primary" disabled={isLoading} type="submit">
                    Rechercher
                </button>
            </form>
            {error && <div className="form-error">{error}</div>}
            <Loader isLoading={isLoading} />
        </>
    )
}
