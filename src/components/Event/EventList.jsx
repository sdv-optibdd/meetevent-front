import EventCard from "./EventCard.jsx";
import { useFetchEvents } from "../../hooks/useFetchEvents.js";
import Loader from "../Loader.jsx";
import { useAppStore } from "../../store/store.js";

export default function EventList() {
    const { events } = useAppStore()
    const { isLoading, error } = useFetchEvents('/party');

    return (
        <>
            <h2 className="mb-4 text-xl font-bold">Les évènements autour de toi</h2>
            {error && <div className="form-error">{error}</div>}
            <Loader isLoading={isLoading} />
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
                {events && events.map(event =>
                    <EventCard key={event.id} event={event} />
                )}
            </div>
            {events && ( events.length === 0) &&
                <p>Aucune résultat.</p>
            }
        </>

    )
}