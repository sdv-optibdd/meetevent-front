import { apiBaseURL } from "../../config.js";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);
    const navigate = useNavigate();

    const handleLogin = async (e) => {
        e.preventDefault();
        setError(null);

        try {
            const response = await fetch(`${apiBaseURL}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: username,
                    password: password
                })
            });

            if (!response.ok) {
                const errorData = await response.json();
                const errorMessage = errorData.message || 'Login failed';
                throw new Error(errorMessage);
            }

            const data = await response.json();
            localStorage.setItem('token', data.token);
            setUsername('');
            setPassword('');
            navigate('/');

        } catch (error) {
            console.error('Error:', error.message);
            setError(error.message);
            setPassword('');
        }
    };

    return (
        <>
            <div className="pt-20">
                <form onSubmit={handleLogin} className="max-w-xl mx-auto border p-10">
                    {error && <div className="form-error">{error}</div>}
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            className="form-control"
                            value={username}
                            onChange={(e) => {
                                setUsername(e.target.value);
                                setError(null);
                            }}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Mot de passe</label>
                        <input
                            type="password"
                            id="password"
                            className="form-control"
                            value={password}
                            onChange={(e) => {
                                setPassword(e.target.value);
                                setError(null);
                            }}
                        />
                    </div>
                    <button type="submit" className="btn-primary">Connexion</button>
                </form>
            </div>
        </>
    );
}
