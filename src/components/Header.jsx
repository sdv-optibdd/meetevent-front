
export default function Header() {

    return (
        <div className="flex items-center justify-between bg-slate-100 py-3 px-5">
            <a href="" className="text-xl font-bold">
                MeetEvent
            </a>
            <nav>
                <ul className="flex items-center gap-4">
                    <li><a href="/">Events</a></li>
                    <li><a href="/login">Se connecter</a></li>
                </ul>
            </nav>
        </div>
    )
}