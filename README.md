## FRONTEND MEETEVENT

Ce projet est la partie frontend de l'application.

### Pré-requis 
- NodeJS

### Installation

Pour installer l'application il suffit de lancer la commande suivante: 
```bash 
npm install
```

Pour démarrer l'application

```bash
npm run dev
```

### Enjoy !!!🚀

L'application est désormais accessible sur 
[http://localhost:5173](http://localhost:5173)